#include <LiquidCrystal_PCF8574.h>
#include <Wire.h>


LiquidCrystal_PCF8574 lcd(0x3F);

const int trigPin_A = 3;
const int echoPin_A = 2;

const int trigPin_B = 7;
const int echoPin_B = 6;

const int trigPin_C = 12;
const int echoPin_C = 11;

void setup () {
    Serial.begin (9600);
    
    pinMode(trigPin_A, OUTPUT);
    pinMode(echoPin_A, INPUT);

    pinMode(trigPin_B, OUTPUT);
    pinMode(echoPin_B, INPUT);

    pinMode(trigPin_C, OUTPUT);
    pinMode(echoPin_C, INPUT);
    
    lcd.setBacklight(255);
    lcd.begin(16,2);
}

void loop () {
    float distance_A = getDistance(trigPin_A, echoPin_A);
    float distance_B = getDistance(trigPin_B, echoPin_B);
    float distance_C = getDistance(trigPin_C, echoPin_C);
 
    printLCD(String(distance_A), String(distance_B), String(distance_C));
    
    delay (1000);
}

float getDistance(int trig, int echo) {
    float tmpDistance;
    long tmpDuration;
    digitalWrite(trig, LOW);
    delayMicroseconds(2);
 
    digitalWrite(trig, HIGH);
    delayMicroseconds(10);
    digitalWrite(trig, LOW);
 
    tmpDuration = pulseIn(echo, HIGH);
 
    tmpDistance = tmpDuration * 0.034 / 2.0;

    Serial.print(tmpDistance);
    Serial.println(" cm");
    return tmpDistance;
}

int tmpLen = 0;

void printLCD(String rang_a, String rang_b, String rang_c) {
    rang_a.remove(rang_a.length() - 1);
    rang_b.remove(rang_b.length() - 1);
    rang_c.remove(rang_c.length() - 1);
    
    int lenTotals = rang_a.length() + rang_b.length() + rang_c.length();
    if(tmpLen != 0) {
      if (tmpLen - lenTotals != 0) {
        lcd.clear();
      }
    }

    tmpLen = rang_a.length() + rang_b.length() + rang_c.length();
//    lcd.clear();
    lcd.setCursor(4,0);
    lcd.print("A: ");
    lcd.print(rang_a);

  

    lcd.setCursor(4,1);
    lcd.print("B: ");
    lcd.print(rang_b);

    lcd.setCursor(4,2);
    lcd.print("C: ");
    lcd.print(rang_c);

    lcd.setCursor(13,0);
    lcd.print("CM");
    lcd.setCursor(13,1);
    lcd.print("CM");
    lcd.setCursor(13,2);
    lcd.print("CM");
}



/******** end code ********/
